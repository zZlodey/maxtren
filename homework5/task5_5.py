# В массиве целых чисел с количеством элементов 19 определить максимальное число и
# заменить им все четные по значению элементы. [02-4.1-BL19]
def changed_by_max_value(array):

    for i in range(len(array)):
        if array[i] % 2 == 0:
            array[i] = max(array)

    return array
print(changed_by_max_value([1, 6, 5, 6, 6, 6, 7, 7, 5.8, 100, 1, 12, 53, 84, 15, 17, 10, 8, 99]))