# Написать программу, в которой вводятся два операнда Х и Y и знак операции sign (+, –, /, *).
# Вычислить результат Z в зависимости от знака.
# Предусмотреть реакции на возможный неверный знак операции, а также на ввод Y=0 при делении.
# Организовать возможность многократных вычислений без перезагрузки программа (т.е. построить бесконечный цикл).
# В качестве символа прекращения вычислений принять ‘0’ (т.е. sign='0').

# while True:
#     sing = input('Что хотим сделать? : ')
#
#     if sing == '0':
#         print('Выход')
#         break
#     if sing in ('+', '-', '/', '*'):
#         x = int(input('Введите первое число: '))
#         y = int(input('Введите второе число: '))
#         if sing == '+':
#             z = x + y
#             print('Результат сложения ', z)
#         elif sing == '-':
#             z = x - y
#             print('Результат: ', z)
#         elif sing == '/':
#             if y == 0:
#                 print('На 0 делить нельзя!')
#                 continue
#             z = x / y
#             print('Результат: ', z)
#
#         elif sing == '*':
#             z = x * y
#             print('Результат: ', z)
#     else:
#         print('Не верный символ!')

def simple_calc(x, y, operator):
    while True:

        if operator == '0':
            print('Выход')
            break
        if operator in ('+', '-', '/', '*'):
            if operator == '+':
                z = int(x) + int(y)
                print('Результат сложения ', z)
                break
            elif operator == '-':
                z = int(x) - int(y)
                print('Результат: ', z)
                break
            elif operator == '/':
                if y == 0:
                    print('На 0 делить нельзя!')
                    break
                z = int(x) / int(y)
                # print('Результат: ', z)
                break
            elif operator == '*':
                z = int(x) * int(y)
                break
        else:
            print('Не верный символ!')
    return z


print(simple_calc('1', '2', "*"))
