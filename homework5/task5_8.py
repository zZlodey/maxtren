# В заданной строке расположить в обратном порядке все слова. Разделителями слов считаются пробелы. [02-7.2-HL08]
def str_reverse(stroka):
    a = stroka.split(' ')
    a.reverse()

    return ' '.join(a)
print(str_reverse("Lorem ipsum dolor sit amet consectetur adipiscing elit"))
print(str_reverse("Улыбок тебе, дед Макар"))
# stroka = "Lorem ipsum dolor sit amet consectetur adipiscing elit"
# b = stroka.split(' ')
# b.reverse()
# print(' '.join(b))
