# tovar = {
#     '1': 25,
#     '2': 28,
#     '3': 3}
# name = {
#     '4': 55,
#     '5': 48,
#     '6': 66}
# city = {
#     '7': 44,
#     '8': 89,
#     '9': 51}
# alldict = {}
# alldict = tovar | name | city
# # alldict = {**tovar, **name, **city}
# alldict = sorted(alldict.items(), key= lambda x: x[1])
# alldict = dict(alldict)
# print(alldict)

# stroka = "String will never be empty and you a do not need to account for different data types"
# str2 = stroka.split()
# koli_vo = 0
# dict = {}
# for i in str2:
#     koli_vo = str2.count(i)
#     dict[i] = koli_vo
# print(dict)
#


# print(str2)
# dlina_min = 999
# dlina_max = 0
# for i in str2:
#     if len(i) < dlina_min:
#         dlina_min = len(i)
#         korotkoe = i
#     if len(i) > dlina_max:
#         dlina_max = len(i)
#         dlinnoe = i
# print(korotkoe)
# print(dlinnoe)

# karta = '4455 4855 4778 9965'
# new = ''
# for i in range(len(karta) -4):
#     if karta[i] != ' ':
#         new += '*'
#     else:
#         new += ' '
# new += karta[-4:]
# a = 'шалаШ'
# b = a.lower()
# if b == b[::-1]:
#     print('yes')
# else:
#     print('no')

# choice = "y"
#
# while choice.lower() == "y":
#     print("Привет")
#     choice = input("Для продолжения нажмите Y, а для выхода любую другую клавишу: ")
# print("Работа программы завешена")
# number = int(input("Введите число: "))
# i = 1
# factorial = 1
# while i <= number:
#     factorial *= i
#     i += 1
# print("Факториал числа", number, "равен", factorial)
# spisok= [10, 40, 20, 30]
# i = 0
# for element in spisok:
#     spisok[i] = element +2
#     i+=1
# print(spisok)
# a = 5
# while a > 0:
#    if a % 3 == 0:
#        break
#    print(a)
#    a -= 1
# print(a)
# a = 5
# while a > 0:
#    if a % 3 == 0:
#        a -= 1
#        continue
#    print(a)
#    a -= 1
# print(a)
# n = 15
# for i in range(n):
#    print(i**2)
# print(n)
# for i in [[1,2,3],
#           [4,5,6],
#           [7,8,9]]:
#    for j in i:
#        print(j)
# for i, elem in enumerate(['a','b','c','d']):
#    print(f'{i} - {elem}')
#
# a = [1, 2, 3, 3, 3, 4, 4, 5, 6, 7, 8, 9, 10]
# for i in a:
#     if i < 5:
#         print(i)
#     elif i > 5 and i < 8:
#         print(i**2)
#     else:
#         print(i**3)
# def hello_name(name):
#     return f'Hello, {name}'
#
#
# all_name = ['Alex', 'Sasha', 'Dima', 'Igor', 'Ira']
#
# for i in all_name:
#     hello_name(i)
#     print(hello_name(i))

def my_func(*args):
    summ = 0
    summ_str = ''
    for i in args:
        if str(i).isdigit():
            summ += i
        else:
            summ_str += i
    return summ, summ_str
