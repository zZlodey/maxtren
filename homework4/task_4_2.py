# Дан список целых чисел. Посчитать сколько четных чисел в списке.
# цикл for

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
b = []

# for i in a:
#     if i % 2 == 0:
#         b.append(i)
# print(len(b))

# цикл while
index = 0
while index < len(a[:]):
    if a[index] % 2 == 0:
        b.append(a[index])
    index += 1
print(b)



