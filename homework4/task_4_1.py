# 1) Дан список целых чисел.
# Создать новый список, каждый элемент которого равен исходному элементу умноженному на -2


# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# b = []
# # index = 0           #index - счетчик, чтобы while не был бесконечным циклом, его же обязательно меняем
# # while index < len(a[:]):    #while index < len[a] - смотрим. меньше ли счетчик чем длина списка, если да то:
# #     if index >= 0:          #проверяем if index >=0 чтобы не было отрицательных значений( иначе -2 * -2 = 2)
# #       b.append(a[index] * -2)   #и добавляем в новый список, меняя значение index (он же перебор значений) * -2
# #     index += 1        #index += 1 меняем счетчик, чтобы цикл продолжал переберать элементы
# #     print(b)
#
# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# b = []
# for i in a:
#     b.append(i * -2)
# print(b)


