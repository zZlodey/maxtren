# # Функция принимает имя и выводит на экран.
# # Добавляем еще 5 имен и церез цикл функуции
# # выводим
# def hello_name(name):
#     return f'Hello, {name}'
#
#
# all_name = ['Alex', 'Sasha', 'Dima', 'Igor', 'Ira']
#
# for i in all_name:
#     hello_name(i)
#     print(hello_name(i))
# def my_pow(number, power):
#     result = number ** power + 1
#     return result
# result = my_pow(power=3, number=5)
# print(result)
def my_func(*args):
    summ = 0
    for i in args:
        summ = summ + i
    return summ
summ = my_func(1, 2, 4)
print(summ)

def my_func(*args):
    summ = 0
    summ_str = ''
    for i in args:
        if str(i).isdigit():
            summ += i
        else:
            summ_str += i
    return summ, summ_str
summ = my_func(1, 2, 4, 'sssaa')
print(summ)