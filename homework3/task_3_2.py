# В семье N свадьба. Они решили выбрать заведение, где будут праздновать в зависимости от количества людей,
# которое прибудет к утру.
# Если их будет больше 50 - закажут ресторан, если от 20 до 50 -то кафе, а если меньше 20 то отпраздную дома.
# Вывести "ресторан", "кафе", "дом" в зависимости от количества гостей (прочитать переменную с консоли)


# family = int(input("Сколько человек: "))

# if family >= 50:
#     print("Ресторан")
# elif family >= 20:
#     print("Кафе")
# else:
#     print("??")

def wedding(people_amount):
    if people_amount >= 50:
        return "Ресторан"
    elif people_amount >= 20:
        return "Кафе"
    elif people_amount < 20:
        return 'Дом'
    else:
        return '??'
    return people_amount


print(wedding(35))
