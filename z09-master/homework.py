import copy
import math
import random


# Task 1.1
def operations(a, b):
    return a + b, a - b, a * b


# Task 1.2
def counter(x, y):
    return (x - y) / (1 + (x * y))


# Task 1.3
def cube(l):
    x = l ** 2 * 6
    y = l ** 3
    return y, x


# Task 1.4
def mean(a, b):
    z = (a + b) / 2
    return z


def geometric_mean(a, b):
    srgr = pow((a * b), 0.5)
    return srgr


# Task 1.5
def hypotenuse(a, b):
    q = a ** 2 + b ** 2
    return pow(q, 0.5)


def triangle_areas(a, b):
    z = ((a * b) / 2)
    return z


# Task 2.1
def symbol_3(string):
    return string[2]


# Task 2.2
def sec_symbol_from_end(string):
    return string[-2]


# Task 2.3
def first_five(string):
    return string[:5]


# Task 2.4
def without_two_last_symbols(string):
    return string[0:-2]


# Task 2.5
def even_symbols(string):
    return string[::2]


# Task 3.1
def millennium(number):
    if number % 1000 == 0:
        return 'millennium'
    else:
        return False


# Task 3.2
def wedding(people_amount):
    if people_amount >= 50:
        return "Ресторан"
    elif people_amount >= 20:
        return "Кафе"
    elif people_amount < 20:
        return 'Дом'
    else:
        return '??'
    return people_amount


# # Task 3.3
def str_len(string):
    return


# # Task 3.4

def print_center_letter(string):
    return


# # Task 4.1
def mult_neg_2(some_list):
    return


# # Task 4.2
def even_list(some_list):
    return


# # Task 4.3
def dict_edit(dictionary):
    return


# # Task 4.4
def shift_list(some_list):
    return


# # Task 4.5
def fibonacci_list():
    return


# # Task 5.1

def simple_calc(x, y, operator):
    while True:

        if operator == '0':
            print('Выход')
            break
        if operator in ('+', '-', '/', '*'):
            if operator == '+':
                z = int(x) + int(y)
                print('Результат сложения ', z)
                break
            elif operator == '-':
                z = int(x) - int(y)
                print('Результат: ', z)
                break
            elif operator == '/':
                if y == 0:
                    print('На 0 делить нельзя!')
                    break
                z = int(x) / int(y)
                # print('Результат: ', z)
                break
            elif operator == '*':
                z = int(x) * int(y)
                break
        else:
            print('Не верный символ!')
    return z


# # Task 5.2

def sum_elements_of_numbers(nmb):
    return


# # Task 5.3
def friendly_checker():
    return


# # Task 5.4
def get_sum(num):
    return


# # Task 5.5
def changed_by_max_value(array):
    for i in range(len(array)):
        if array[i] % 2 == 0:
            array[i] = max(array)
    return array


# Task 5.6
def incr(sp):
    return


# Task 5.7
def change_number(matrix):
    return


# Task 5.8
def str_reverse(stroka):
    a = stroka.split(' ')
    a.reverse()

    return ' '.join(a)


# # Task 6.1
def create_matrix(start_number, end_number, num_rows, num_columns):
    return


# # Task 6.2
def get_max_number(array):
    return


# # Task 6.3
def get_min_number(array):
    return


# # Task 6.4
def get_sum_all_numbers(array):
    return


# # Task 6.5
def get_index_row_max_sum_numbers(array):
    return


# # Task 6.6
def get_index_column_max_sum_numbers(array):
    return


# # Task 6.7
def get_index_row_min_sum_numbers(array):
    return


# # Task 6.8
def get_index_column_min_sum_numbers(array):
    return


# # Task 6.9
def set_zero_elements_above_main_diagonal(array):
    return


# # Task 6.10
def set_zero_elements_below_main_diagonal(array):
    return


# # Task 6.11
def create_two_matrix(m, n):
    return


# # Task 6.12
def get_summ_matrix(matrix_a, matrix_b):
    return


# # Task 6.13
def get_sub_matrix(matrix_a, matrix_b):
    return


# # Task 6.14
def get_multipl_g_matrix(g, matrix):
    return


# # Task 7

def inch_to_cm(inch):
    return


def cm_to_inch(cm):
    return


def ml_to_km(mm):
    return


def km_to_ml(km):
    return


def ft_to_kg(ft):
    return


def kg_to_ft(ft):
    return


def oz_to_g(oz):
    return


def g_to_oz(g):
    return


def gallon_to_l(gallon):
    return


def l_to_gallon(l):
    return


def pint_to_l(pint):
    return


def l_to_pint(l):
    return


# Task 8.1
def double_factorial(x):
    return


# Task 8.2
def polindrom(word):
    return word == word[::-1]


def polindrom_in_polindroms(list_of_words):
    return


# Task 8.3
def sin1(x, eps):
    return


def factorial(n):
    return


def list_sin_with_different_precision(x, list_eps):
    return


# Task 9.1
def format(list):
    return


# Task 9.2
def doubling_arguments(**kwargs):
    return


# Task 9.3
def decorator(fun):
    return


def numbers(list_numbers):
    return


# Task 9.4
def decorator_reverse_args(fun):
    return


def decorating_function(*args):
    return
