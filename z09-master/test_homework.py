from .homework import *
from .utils import random_string_uppercase, random_string_letters, random_int_big, random_int_small, \
    random_string_lowercase
import datetime


def test_operations():
    assert operations(2, 2) == (4, 0, 4)
    assert operations(3, 2) == (5, 1, 6)
    assert operations(2, 7) == (9, -5, 14)


def test_counter():
    assert counter(2, 2) == 0.0
    assert counter(3, 2) == 0.14285714285714285
    assert counter(2, 7) == -0.3333333333333333


def test_cube():
    assert cube(2) == (8, 24)
    assert cube(3) == (27, 54)
    assert cube(7) == (343, 294)


def test_mean():
    assert mean(1, 1) == 1
    assert mean(2, 3) == 2.5
    assert mean(6, 9) == 7.5
    assert mean(10000, 100100) == 55050.0


def test_geometric_mean():
    assert geometric_mean(10, 10) == 10.0
    assert geometric_mean(0, 4) == 0.0
    assert geometric_mean(1, 8) == 2.8284271247461903
    assert geometric_mean(-5, -6) == 5.477225575051661
    assert geometric_mean(20, 1) == 4.47213595499958


def test_hypotenuse():
    assert hypotenuse(10, 20) == 22.360679774997898
    assert hypotenuse(0.5, 2.0) == 2.0615528128088303
    assert hypotenuse(1.0, 200) == 200.0024999843752
    assert hypotenuse(-10, 20) == 22.360679774997898
    assert hypotenuse(10, 10) == 14.142135623730951


def test_triangle_areas():
    assert triangle_areas(1, 3) == 1.5
    assert triangle_areas(1, 0.000000001) == 5e-10
    assert triangle_areas(10, 1) == 5.0
    assert triangle_areas(5, 180) == 450.0
    assert triangle_areas(0.1225489798, 9.321698795654) == 0.5711823387051431


def test_symbol_3():
    string = random_string_letters()
    assert symbol_3(string) == string[2]
    string = random_string_uppercase() + string
    assert symbol_3(string) == string[2]
    string = random_string_letters()
    assert symbol_3(string) == string[2]
    string = str(random_int_big())
    assert symbol_3(string) == string[2]


def test_sec_symbol_from_end():
    string = random_string_letters()
    assert sec_symbol_from_end(string) == string[-2]
    string = random_string_uppercase()
    assert sec_symbol_from_end(string) == string[-2]
    string = str(random_int_big())
    assert sec_symbol_from_end(string) == string[-2]


def test_first_five():
    string = random_string_letters()
    assert first_five(string) == string[0:5]
    string = random_string_uppercase()
    assert first_five(string) == string[0:5]
    string = str(random_int_big())
    assert first_five(string) == string[0:5]


def test_without_two_last_symbols():
    string = random_string_letters()
    without_two_last = string[0:-2]
    assert without_two_last_symbols(string) == without_two_last
    assert without_two_last_symbols(string) == without_two_last
    assert without_two_last_symbols(string) == without_two_last
    assert without_two_last_symbols(string) == without_two_last
    assert without_two_last_symbols(string) == without_two_last


def test_even_symbols():
    string = random_string_letters()
    assert even_symbols(string) == string[::2]
    string = random_string_uppercase()
    assert even_symbols(string) == string[::2]
    string = str(random_int_big())
    assert even_symbols(string) == string[::2]


def test_millennium():
    assert millennium(10001000) == 'millennium'
    assert millennium(random_int_big()) == False
    assert millennium(1101110111000000) == 'millennium'
    assert millennium(random_int_small()) == False
    assert millennium(random_int_small()) == False
    assert millennium(random_int_small()) == False


def test_wedding():
    assert wedding(random_int_big()) == 'Ресторан'
    assert wedding(random_int_small()) == 'Дом'
    assert wedding(35) == 'Кафе'
    assert wedding(random_int_big()) == 'Ресторан'


def test_str_len():
    string = random_string_lowercase()
    assert str_len(string) == string[1]
    string = str(random_int_big())
    assert str_len(string) == string[1]
    string = random_string_uppercase()
    assert str_len(string) == string + '!!!'


def test_print_center_letter():
    assert print_center_letter('!9qw^098&w1s123#4fds') == 'w'
    assert print_center_letter('01100') == '1'
    assert print_center_letter('0210') == '2'
    assert print_center_letter('0210002') == ('0', '21000')
    assert print_center_letter('10100') == ('1', '010')


def test_mult_neg_2():
    assert mult_neg_2([1, -2, 9, 4, -5, 0, -0.011]) == [-2, 4, -18, -8, 10, 0, 0.022]
    assert mult_neg_2([-4, 1, -1, 1, -1, 0, -0.00001]) == [8, -2, 2, -2, 2, 0, 2e-05]
    assert mult_neg_2([4, -2, -9, -4.002200100, -5, 0, -0.00001]) == [-8, 4, 18, 8.0044002, 10, 0, 2e-05]
    assert mult_neg_2([0, -1.8, 9, 4, -5, 0, -1.00001]) == [0, 3.6, -18, -8, 10, 0, 2.00002]


def test_even_list():
    assert even_list([2, 4, 2.2, 6, 9, 12, 14, 0, 15, 1, 8, 7, 9, ]) == [2, 4, 6, 12, 14, 0, 8]
    assert even_list([-4, 1, -1, 1, -1, 0, 0]) == [-4, 0, 0]
    assert even_list([4, -2, -9]) == [4, -2]
    assert even_list([-1.8, 9, 4, 5, 0, 2, 2]) == [4, 0, 2, 2]


def test_dict_edit():
    assert dict_edit({'python': 'value 1', 'america': 'eur', '789': '789', '00.0': 'rub'}) == \
           {'python6': 'value 1', 'america7': 'eur', '7893': '789', '00.04': 'rub'}
    assert dict_edit({'sql': 'value 1', 'europe': 'eur', 'dollar': 'aaa', 'MOSCOW': 'rub'}) == \
           {'sql3': 'value 1', 'europe6': 'eur', 'dollar6': 'aaa', 'MOSCOW6': 'rub'}
    assert dict_edit({'qwerty': 'value 1', 'europe': 'gta', '987': 'usd', 'ruble': 'rub'}) == \
           {'qwerty6': 'value 1', 'europe6': 'gta', '9873': 'usd', 'ruble5': 'rub'}
    assert dict_edit({'test': 'value 1', 'aezakmi': 'eur', 'Sca': 'usd', 'ruble': 'rub'}) == \
           {'test4': 'value 1', 'aezakmi7': 'eur', 'Sca3': 'usd', 'ruble5': 'rub'}


def test_shift_list():
    assert shift_list([1, 2, 3, 4, 5]) == [2, 3, 4, 5, 1]
    assert shift_list([2, 3, 4, 5, 1]) == [3, 4, 5, 1, 2]


def test_fibonacci_list():
    assert fibonacci_list() == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610]


def test_simple_calc():
    assert simple_calc('1', '1', "+") == 2
    assert simple_calc('1', '1', "-") == 0
    assert simple_calc('1', '0', "/") == 'ZeroDivisionError'
    assert simple_calc('1', '2', "*") == 2


def test_sum_elements_of_numbers():
    assert sum_elements_of_numbers(654) == (15, 120)
    assert sum_elements_of_numbers(5165) == (17, 150)
    assert sum_elements_of_numbers(1) == (1, 1)
    assert sum_elements_of_numbers(123456789) == (45, 362880)
    assert sum_elements_of_numbers(321654987) == (45, 362880)
    assert sum_elements_of_numbers(5165 + 3658 + 6985) == (22, 0)


def test_friendly_checker():
    assert friendly_checker() == [(220, 284), (284, 220)]


def test_get_sum():
    assert get_sum(1) == 1
    assert get_sum(1.5) == 1
    assert get_sum(5) == 2.283333333333333
    assert get_sum(123) == 5.393459552775602
    assert get_sum(0) == 0


def test_changed_by_max_value():
    assert changed_by_max_value([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 13, 14, 15, 16, 17, 18, 19]) == [1, 19, 3, 19, 5,
                                                                                                        19, 7, 19, 9,
                                                                                                        19, 11, 1, 13,
                                                                                                        19, 15, 19, 17,
                                                                                                        19, 19]
    assert changed_by_max_value([1, 6, 5, 6, 6, 6, 7, 7, 5.8, 100, 1, 12, 53, 84, 15, 17, 10, 8, 99]) == [1, 100, 5,
                                                                                                          100, 100, 100,
                                                                                                          7, 7, 5.8,
                                                                                                          100, 1, 100,
                                                                                                          53, 100, 15,
                                                                                                          17, 100, 100,
                                                                                                          99]
    assert changed_by_max_value([1, 87, 6, 4, 51, 6, 7, 80, 19, 6, 1, 2, 3, 34, 5, 76, 47, 28, 19]) == [1, 87, 87, 87,
                                                                                                        51, 87, 7, 87,
                                                                                                        19, 87, 1, 87,
                                                                                                        3, 87, 5, 87,
                                                                                                        47, 87, 19]


def test_change_number():
    assert change_number([[4, 1, 4.1], [8, 3, 2], [52, 75, 8]]) == [
        [4.1, 1, 4], [3, 8, 2], [52, 8, 75]]
    assert change_number([[4, 1, 2, 1], [3, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == [
        [4, 1, 2, 1], [3, 10, 2, 8], [52, 8, 75, 26], [82, 8, 75, 100]]
    assert change_number([[41, 1, 4.5], [1, 2, 3], [5, 7, 8]]) == [
        [41, 1, 4.5], [1, 3, 2], [5, 7, 8]]


def test_str_reverse():
    assert str_reverse("Lorem ipsum dolor sit amet consectetur adipiscing elit") == \
           'elit adipiscing consectetur amet sit dolor ipsum Lorem'
    assert str_reverse("Улыбок тебе, дед Макар") == \
           'Макар дед тебе, Улыбок'
    assert str_reverse("elit adipiscing consectetur amet sit dolor ipsum Lorem") == \
           'Lorem ipsum dolor sit amet consectetur adipiscing elit'


def test_dividers():
    assert dividers(100, 105) == {100: [2, 4, 5, 10, 20, 25, 50], 101: [], 102: [2, 3, 6, 17, 34, 51], 103: [],
                                  104: [2, 4, 8, 13, 26, 52], 105: [3, 5, 7, 15, 21, 35]}
    assert dividers(205, 217) == {205: [5, 41], 206: [2, 103], 207: [3, 9, 23, 69], 208: [2, 4, 8, 13, 16, 26, 52, 104],
                                  209: [11, 19], 210: [2, 3, 5, 6, 7, 10, 14, 15, 21, 30, 35, 42, 70, 105], 211: [],
                                  212: [2, 4, 53, 106], 213: [3, 71], 214: [2, 107], 215: [5, 43],
                                  216: [2, 3, 4, 6, 8, 9, 12, 18, 24, 27, 36, 54, 72, 108], 217: [7, 31]}
    assert dividers(10378, 10381) == {10378: [2, 5189], 10379: [97, 107],
                                      10380: [2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60, 173, 346, 519, 692, 865, 1038,
                                              1730, 2076, 2595, 3460, 5190], 10381: [7, 1483]}


def test_info_trains():
    assert info_trains(
        {1: [["Minsk", datetime.datetime(2021, 4, 28, 22, 0)], ["Gomel", datetime.datetime(2021, 4, 28, 19, 0)]], 2: [
            ["Minsk", datetime.datetime(2021, 4, 29, 8, 15)], ["Moskow", datetime.datetime(2021, 4, 28, 19, 0)]]}) == {
               2: [['Minsk', datetime.datetime(2021, 4, 29, 8, 15)], ['Moskow', datetime.datetime(2021, 4, 28, 19, 0)]]}

    assert info_trains({1: [["Minsk", datetime.datetime(2021, 3, 3, 3, 0)],
                            ["Kiev", datetime.datetime(2021, 3, 3, 23, 28)]]}) == 'Incorrect data!'
    assert info_trains(
        {1: [["SPB", datetime.datetime(2021, 5, 11, 22, 14)], ["Minsk", datetime.datetime(2021, 5, 11, 6, 18)]], 2: [
            ["Minsk", datetime.datetime(2021, 4, 29, 6, 18)], ["Moskow", datetime.datetime(2021, 4, 28, 16, 24)]],
         3: [["Minsk", datetime.datetime(2021, 3, 3, 3, 0)], ["Grodno", datetime.datetime(2021, 3, 2, 23, 28)]]}) == {
               1: [['SPB', datetime.datetime(2021, 5, 11, 22, 14)], ['Minsk', datetime.datetime(2021, 5, 11, 6, 18)]],
               2: [['Minsk', datetime.datetime(2021, 4, 29, 6, 18)],
                   ['Moskow', datetime.datetime(2021, 4, 28, 16, 24)]]}

    assert info_trains(
        {1: [["Minsk", datetime.datetime(2021, 3, 3, 3, 0)], ["Grodno", datetime.datetime(2021, 3, 2, 23, 28)]]}) == {}


def test_incr():
    assert incr([1, 2, 3, 1, 0.5, 0.5, 1, 2, 3, 6, 1, 2]) == 3
    assert incr([1, 2, 3, 1, 2, 3, 1, 2, 3, 6, 1, 2]) == 4
    assert incr([2, 2, 3, 2, 2, 1, 1, 2, 3, 6, 1, 2]) == 3


#
#
def test_get_max_number():
    assert get_max_number([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 100
    # assert get_max_number([[5, 112, 0, 2], ["41", 17, 9, 10], [2, 700, 0.8, 26], [102, 8, 750, 10]]) == 750
    assert get_max_number([[400, 1, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 1000


#
#
def test_get_min_number():
    assert get_min_number([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 1
    assert get_min_number([[5, 112, 0, 2], [41, 17, 9, 10], [2, 700, 0.8, 26], [102, 8, 750, 10]]) == 0
    assert get_min_number([[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 1.2


#
#
def test_get_sum_all_numbers():
    assert get_sum_all_numbers([[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 2975.2
    # assert get_sum_all_numbers([[5, 112, 0, 2], ["41", 17, 9, 10], [2, 700, 0.8, 26], [102, 8, 750, 10]]) == 1794.8

    assert get_sum_all_numbers([[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == 522
    assert get_sum_all_numbers([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 487


#
#
def test_get_index_row_max_sum_numbers():
    assert get_index_row_max_sum_numbers(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 2
    # assert get_index_row_max_sum_numbers([[5, 112, 0, 2], ['41', 17, 9, 10], [20, 700, 0.8, 260], [102, 8, 750, 10]]) == 2
    assert get_index_row_max_sum_numbers([[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == 3
    assert get_index_row_max_sum_numbers([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 3


#
#
def test_get_index_column_max_sum_numbers():
    assert get_index_column_max_sum_numbers(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 3
    assert get_index_column_max_sum_numbers([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 0
    assert get_index_column_max_sum_numbers([[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == 1


#
#
def test_get_index_row_min_sum_numbers():
    assert get_index_row_min_sum_numbers(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 1
    assert get_index_row_min_sum_numbers([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 0
    assert get_index_row_min_sum_numbers([[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == 0


#
def test_get_index_column_min_sum_numbers():
    assert get_index_column_min_sum_numbers(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == 2
    assert get_index_column_min_sum_numbers([[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == 2
    assert get_index_column_min_sum_numbers([[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == 0


#

def test_set_zero_elements_above_main_diagonal():
    assert set_zero_elements_above_main_diagonal(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == [[400, 0, 0, 0], [30, 65, 0, 0],
                                                                                         [520, 705, 8, 0],
                                                                                         [82, 8, 75, 1000]]

    assert set_zero_elements_above_main_diagonal([[4, 1, 2], [33, 8, 2], [52, 75, 8]]) == [[4, 0, 0],
                                                                                           [33, 8, 0],
                                                                                           [52, 75, 8]]
    assert set_zero_elements_above_main_diagonal(
        [[40, 12, 33, 10], [30, 65, 2, 10], [20, 75, 8, 26], [8, 8, 75, 100]]) == [[40, 0, 0, 0], [30, 65, 0, 0],
                                                                                   [20, 75, 8, 0], [8, 8, 75, 100]]


#
def test_set_zero_elements_below_main_diagonal():
    assert set_zero_elements_below_main_diagonal(
        [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == [[400, 1.2, 33, 10],
                                                                                         [0, 65, 2, 10], [0, 0, 8, 26],
                                                                                         [0, 0, 0, 1000]]
    assert set_zero_elements_below_main_diagonal(
        [[4, 1, 2, 1], [33, 8, 2, 10], [52, 75, 8, 26], [82, 8, 75, 100]]) == [[4, 1, 2, 1], [0, 8, 2, 10],
                                                                               [0, 0, 8, 26], [0, 0, 0, 100]]

    assert set_zero_elements_below_main_diagonal(
        [[40, 12, 33], [30, 65, 2], [20, 75, 8]]) == [[40, 12, 33], [0, 65, 2], [0, 0, 8]]


def test_get_summ_matrix():
    assert get_summ_matrix([[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]],
                           [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == [
               [800, 2.4, 66, 20], [60, 130, 4, 20], [1040, 1410, 16, 52], [164, 16, 150, 2000]]
    assert get_summ_matrix(
        [[40, 12, 33], [30, 65, 2], [20, 75, 8]], [[40, 12, 33], [0, 65, 2], [0, 0, 8]]) == [
               [80, 24, 66], [30, 130, 4], [20, 75, 16]]
    assert get_summ_matrix(
        [[4, 102, 33], [30, 65, 2], [20, 75, 8]], [[50, 10, 33], [0, 605, 2], [20, 10, 8]]) == [
               [54, 112, 66], [30, 670, 4], [40, 85, 16]]


def test_get_sub_matrix():
    assert get_sub_matrix([[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]],
                          [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == [
               [0, 0.0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

    assert get_sub_matrix(
        [[5, 12, 33], [30, 5, 2], [20, 75, 8]], [[4, 2, 33], [0, 6, 2], [4, 5, 8]]) == [
               [1, 10, 0], [30, -1, 0], [16, 70, 0]]

    assert get_sub_matrix(
        [[4, 102, 33], [30, 65, 2], [20, 75, 8]], [[50, 10, 33], [0, 605, 2], [20, 10, 8]]) == [
               [-46, 92, 0], [30, -540, 0], [0, 65, 0]]


def test_get_multipl_g_matrix():
    assert get_multipl_g_matrix(6, [[400, 1.2, 33, 10], [30, 65, 2, 10], [520, 705, 8, 26], [82, 8, 75, 1000]]) == [
        [2400, 7.199999999999999, 198, 60], [180, 390, 12, 60], [3120, 4230, 48, 156], [492, 48, 450, 6000]]

    assert get_multipl_g_matrix(10, [[4, 2, 33], [0, 6, 2], [4, 5, 8]]) == [[40, 20, 330], [0, 60, 20], [40, 50, 80]]

    assert get_multipl_g_matrix(0, [[4, 102, 33], [30, 65, 2], [20, 75, 8]], ) == [[0, 0, 0], [0, 0, 0], [0, 0, 0]]


def test_inch_to_cm():
    assert inch_to_cm(100) == 254.0
    assert inch_to_cm(0.5) == 1.27
    assert inch_to_cm(1.555688557) == 3.95144893478
    assert inch_to_cm(6665) == 16929.1
    # assert inch_to_cm("10") == 25.4


def test_cm_to_inch():
    assert cm_to_inch(100) == 39.37007874015748
    # assert cm_to_inch("0.5") == 0.19685039370078738
    assert cm_to_inch(1.555688557) == 0.6124758098425197
    assert cm_to_inch(6665) == 2624.015748031496
    assert cm_to_inch(10) == 3.937007874015748


def test_ml_to_km():
    assert ml_to_km(100) == 160.9
    assert ml_to_km(0.5) == 0.8045
    assert ml_to_km(1.555688557) == 2.503102888213
    assert ml_to_km(6665) == 10723.985
    assert ml_to_km(10) == 16.09


def test_km_to_ml():
    assert km_to_ml(100) == 62.15040397762586
    assert km_to_ml(0.5) == 0.3107520198881293
    assert km_to_ml(1.555688557) == 0.9668667228091983
    assert km_to_ml(6665) == 4142.3244251087635
    assert km_to_ml(10) == 6.215040397762586


def test_ft_to_kg():
    assert ft_to_kg(100) == 45.35147392290249
    assert ft_to_kg(0.5) == 0.22675736961451246
    assert ft_to_kg(1.555688557) == 0.7055276902494331
    assert ft_to_kg(6665) == 3022.675736961451
    assert ft_to_kg(10) == 4.535147392290249


def test_kg_to_ft():
    assert kg_to_ft(100) == 220.5
    assert kg_to_ft(0.5) == 1.1025
    assert kg_to_ft(1.555688557) == 3.4302932681850002
    assert kg_to_ft(6665) == 14696.325
    assert kg_to_ft(10) == 22.05


def test_oz_to_g():
    assert oz_to_g(100) == 2834.95
    assert oz_to_g(0.5) == 14.17475
    assert oz_to_g(1.555688557) == 44.102992746671504
    assert oz_to_g(6665) == 188949.41749999998
    assert oz_to_g(10) == 283.495


def test_g_to_oz():
    assert g_to_oz(100) == 3.527399072294044
    assert g_to_oz(0.5) == 0.01763699536147022
    assert g_to_oz(1.555688557) == 0.0548753437274026
    assert g_to_oz(6665) == 235.10114816839803
    assert g_to_oz(10) == 0.3527399072294044


def test_gallon_to_l():
    assert gallon_to_l(100) == 378.5
    assert gallon_to_l(0.5) == 1.8925
    assert gallon_to_l(1.555688557) == 5.888281188245
    assert gallon_to_l(6665) == 25227.025
    assert gallon_to_l(10) == 37.85


def test_l_to_gallon():
    assert l_to_gallon(0.5) == 0.1321003963011889
    assert l_to_gallon(1.5) == 0.3963011889035667
    assert l_to_gallon(1.555688557) == 0.4110141498018494
    assert l_to_gallon(6665) == 1760.898282694848
    assert l_to_gallon(10) == 2.642007926023778


def test_pint_to_l():
    assert pint_to_l(100) == 47.326076668244205
    assert pint_to_l(0.5) == 0.23663038334122102
    assert pint_to_l(1.555688557) == 0.736246359204922
    assert pint_to_l(6665) == 3154.283009938476
    assert pint_to_l(10) == 4.73260766682442


def test_l_to_pint():
    assert l_to_pint(100) == 211.3
    assert l_to_pint(0.5) == 1.0565
    assert l_to_pint(1.555688557) == 3.2871699209410004
    assert l_to_pint(6665) == 14083.145
    assert l_to_pint(10) == 21.13


def test_double_factorial():
    assert double_factorial(7) == 105
    assert double_factorial(6) == 48
    assert double_factorial(9) == 945
    assert double_factorial(10) == 3840


def test_polindrom():
    assert polindrom('kjsdhcjbla') == False
    assert polindrom('1234567654321') == True
    assert polindrom('wertyytrew') == True
    assert polindrom('qWertyytrewq') == False


def test_polindrom_in_polindroms():
    assert polindrom_in_polindroms(
        ['1234', '87652', 'uyrew', 'ttyyuu', 'hdggdd', 'lkaksjcdh']) == "There isn't a polydrome"
    assert polindrom_in_polindroms(
        ['1234', '87652', 'stats', 'ttyyuu', 'hdggdd', 'lkaksjcdh']) == "There is a polydrome"
    assert polindrom_in_polindroms(['r']) == "There is a polydrome"
    assert polindrom_in_polindroms(['ytrerty', '12321']) == "There is a polydrome"


def test_list_sin_with_different_precision():
    assert list_sin_with_different_precision(4.983, [0.00001]) == [-0.9636073289871849]
    assert list_sin_with_different_precision(9, [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001]) == [
        0.413541440727335,
        0.41205034861777035,
        0.41212089908727206,
        0.41212089908727206,
        0.4121184193786729,
        0.41211848666886663]
    assert list_sin_with_different_precision(19287,
                                             [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001,
                                              0.000000000001,
                                              0.000000000000001]) == [-0.6982016210635635, -0.6903353329710821,
                                                                      -0.6909063317687207, -0.6908743316985717,
                                                                      -0.6908757579939172, -0.6908757062278925,
                                                                      -0.6908757062278925, -0.69087570774861,
                                                                      -0.6908757077485941]
    assert list_sin_with_different_precision(6344.297635, [0.000005, 0.07, 0.124]) == [-0.9889601717263998,
                                                                                       -0.9834561135482357,
                                                                                       -0.9834561135482357]
    assert list_sin_with_different_precision(345.843, [0.5, 0.0000067, 0.0000065, 0.045, 0.14]) == [
        0.26780810512286024,
        0.26461831312795175,
        0.26461831312795175,
        0.26460685285183083,
        0.26460685285183083]


def test_format():
    assert format([0, 1, 2, 3, 4, 5, 6, 7]) == ['1 - 0', '2 - 1', '3 - 2', '4 - 3', '5 - 4', '6 - 5', '7 - 6',
                                                '8 - 7']
    assert format([4 - 3, 2, '3', 3 + 1, '5', 6, 7]) == ['1 - 1', '2 - 2', '3 - 3', '4 - 4', '5 - 5', '6 - 6',
                                                         '7 - 7']
    assert format(['Kate', 'Bob', 'John', 'Ann', 'Nick']) == ['1 - Kate', '2 - Bob', '3 - John', '4 - Ann',
                                                              '5 - Nick']


def test_doubling_arguments():
    assert doubling_arguments(a=1, b=2, c=7, d=23, e=3, f=5, g='34', h='gtr') == {'aa': 1, 'bb': 2, 'cc': 7,
                                                                                  'dd': 23,
                                                                                  'ee': 3, 'ff': 5, 'gg': '34',
                                                                                  'hh': 'gtr'}
    assert doubling_arguments(qwer='qwer', list=[1, 2, 3], int=123, float=1.2, tuple=(1, 2, 3),
                              dict={1: 2, 2: 1}) == {
               'qwerqwer': 'qwer', 'listlist': [1, 2, 3], 'intint': 123, 'floatfloat': 1.2, 'tupletuple': (1, 2, 3),
               'dictdict': {1: 2, 2: 1}}


def test_numbers():
    assert numbers([2, 4, 5, 7, 0, 6, 8, 11, 22, 34]) == [5, 7, 11]
    assert numbers([5, 55, 66, 77, 88, 3245, 3425, 456, 5654, 9347]) == [5, 55, 77, 3245, 3425, 9347]
    assert numbers([0, 0, 0, 0, 0, 0, 22, 44, 66, 74, 22]) == []


def test_decorating_function():
    assert decorating_function(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) == (10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
    assert decorating_function('start', '1', 2, '3', 4, [5, 6, 7], {8, 8, 8}, (9, 10, 11), 'end') == (
        'end', (9, 10, 11), {8}, [5, 6, 7], 4, '3', 2, '1', 'start')
    assert decorating_function('4', 5, 6, '7', 76 - 83 * 11) == (-837, '7', 6, 5, '4')
    assert decorating_function() == ()
    assert decorating_function(0) == (0)
    assert decorating_function(1, 2) == (2, 1)


