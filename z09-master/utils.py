from random import choice, randint
from string import ascii_uppercase, ascii_letters, ascii_lowercase


def random_string_uppercase():
    return ''.join(choice(ascii_uppercase) for i in range(20))


def random_string_lowercase():
    return ''.join(choice(ascii_lowercase) for i in range(10))


def random_string_letters():
    return ''.join(choice(ascii_letters) for i in range(20))


def random_int_big():
    return randint(10000, 10000000000)


def random_int_small():
    return randint(1, 20)
