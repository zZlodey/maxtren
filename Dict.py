# Dict - Словари - это неупорядоченная коллекция произвольных объектов с доступом по ключу
# Так же называется "Ассоциативным массивом" либо "Ассоциативным списком" (изменяемое )

a = ['moskva', 'piter', 'penza']  # Словари начинают счет с 0. [0, 1, 2...]

d = {
    # key: value    ключи и его значение
    # так мы присваеваем ключам их значение
    'moskva': 495,      # ключ может быть строкой, либо числом (неизменяемы)
    'piter': 812,       # значеие может быть любым
    'penza': 8412
}
print(d)

r = dict(moskva=495, piter=812, penza=8412)  # Альтернативный способ присваивания значений
print(r)

a = [['moskva', 495], ['piter', 812], ['penza', 8412]]  # Создание словаря сразу со значениями
t = dict(a)
print(t)

q = dict.fromkeys(['a', 'b', 'c'], 100)  # Присваевание всем ключам одно значение 100
print(q)

v = {}  # Создание пустого словаря, либо v = dict()
print(v, type(v))
print(a[1])  # Вывод конкретного ключа (1 - номер значения (0, 1[piter] ... либо
print(t["moskva"])  # Вывод по ключу, тогда выводит только значение

t[4] = 'mosikov'  # Добавили в словарь ключ 4 со значением mosikov. Либо так же переписывается значение
print(t)

# person = {}
# s = "IVANOV IVAN MOSKOV MGU 5 5 4 3 5"
# s = s.split()   # преобразуем строку в список
# person['LastName'] = s[0]       # Присвоили ключу [] значение s[0] (Отсчет начинается с 0)
# person['FirstName'] = s[1]      #
# person['Sity'] = s[2]           #
# person['University'] = s[3]     #
# person['Marks'] = []            #  Создали новый ключ чтобы добавить туда несколько значений
# for i in s[4:]:                 #  Присваеваем переменной i значений s[4:] от 4 и далее до конца списка
#      person['Marks'].append(int(i))     #добавляем (append) в ключ Marks целочисленные значения i
# print(person)

del t[4]   # del удаляет ключ
print(t)

print(len(t))       # так мы находим длину словаря
print(1 in t, 'moskva' in t, 'sochi' not in t)       # проверяем есть ли что то в словаре (True/False)

# проверяем, есть ли ключ в словаре, если нет, то создаем его. Елси есть - то ничего не меняется.
if 'sochi' in t:
    print(t[4])
else:
    t['sochi'] = 322
print(t)

# # так как словарь это коллекция, можно обходить через for
# for key in t:
#     print(key, t[key])

# t.clear()   # очистить словарь
# print(t.get('5'))   # get - получить ключ, если нет такого значение выводит 'None'
# чтобы выводил не None а другой текст пишем
# print(t.get('5', "No have"))

# t.setdefault()  # устанавливает ключ. Если есть - выводит его значение, если нет..
# print(t.setdefault('karaganda', 'gorod'))  # то тогда создает этот ключ с пустым значением

# print(t.pop('moskva'))  # Выдергивает ключ из списка. Далее этот ключ прописываться не будет
# print(t)

# print(t.popitem())  # удаляет случайный ключ из списка. Далее этот ключ прописываться не будет
# print(t)

print(t.keys())  # Значения всех ключей словаря
print(t.values())  # выводит значения всех ключей
# for keys (values) in t.keys (values):   # обход через for
#     print(key (values))

print(t.items())  # выводит пары ключ-значение
for para in t.items():
    print(para)
# либо
print(t.items())
for key, value in t.items():
    print(key, value)
