# 5. Даны катеты прямоугольного треугольника. Найти его гипотенузу и площадь.
# x = 3
# y = 6
# print("Площадь: ", (x*y)/2)
# q = x**2 + y**2 #вычисляем квадрат гипотенузы по суммам квадратов катетов
# print("Гипотенуза: ", q**0.5)
def hypotenuse(a, b):
    q = a ** 2 + b ** 2
    return pow(q, 0.5)
print(hypotenuse(10, 20))

def triangle_areas(a, b):
    z = ((a * b) / 2)
    return z
print(triangle_areas(1, 3))
