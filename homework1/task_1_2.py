# 2. Даны действительные числа x и y. Получить |x|-|y|
#                                           --------
#                                            1 + |xy|
def my_expr (x, y):
    return (abs (x) - abs (y))/(1. + abs (x * y))

line = input ("x y (separated by space):")
x, y, = map (float, line.split())
print ("input is:", x, y, ", result is:", my_expr (x, y))