# List - Список
a = [1, 2, 3, 4, "Hello"]  # список в квадрат. скобках
print(a, type(a))
b = a[:]
print(b)
a.append(5)  # добавить значение
print(a)
a.remove("Hello")  # удалить значение
print(a)
a[0] = 200  # изменить значение
print(a)
print(a[1:3])  # вывести с 1 по 3 значение
b = [5, 6, 7, 8]
a.extend(b)  # добавить значение Б в А
print(a)
a.insert(0, 99)  # изменить значение в конкретное место (0)
print(a)
# a.clear()  # удалить все значения
# print(a)
print(a.index(99))
print(a.count(3))  # сколько раз повторяется

# a = "Hello world"
# print(a.split()) преобразование строку в список

a = [23, 45, 76, 43, 32, 74, 12, 78]
a.sort()  # сортирока по возрастанию
print(a)
a.sort(reverse=True)  # сортировка по убыванию
print(a)

n = int(input())
a = [i for i in range(n)]
print(a)
# Мы создали список который мы можем задавать в ручную
